$("#search").click(() => {
    txtName = $("#txtName").val();
    $.ajax({
        type: "get",
        url: "http://localhost:8558/search/" + txtName,
        dataType: "json",
        beforeSend: function () {
            $('#loadingModal').modal('show');
            $("#display").html("");
        },
        success: function (result) {
            if (result.response === "success") {
                var details = "";
                result.results.forEach((data) => {
                    details += '<div class="col-md-3 mt-2 mb-2" onclick="getSuperhero(' + data.id + ')">'
                        + '<div class="card">'
                        + '<img class="card-img-top" src="' + data.image.url + '" alt="' + data.name + '" height="300px">'
                        + '<div class="card-body">'
                        + '<h5 class="card-title">' + data.name + '</h5>'
                        + '<p class="card-text">' + data.biography['full-name'] + '</p>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                });
                $("#display").html(details);
                $('#loadingModal').modal('hide');
            }
            else if (result.response === "error") {
                $('#loadingModal').modal('hide');
                $('#notFound').modal('show');
            }
        },
    });
});

function getSuperhero(id) {
    $.ajax({
        type: "get",
        url: "http://localhost:8558/" + id,
        dataType: "json",
        beforeSend: function () {
            $('#loadingModal').modal('show');
        },
        success: function (result) {
            if (result.response === "success") {
                console.log(result)
                $("#superheroName").html(result.name)
                $("#heroImage").attr("src", result.image.url)

                $.each(result.powerstats, (key, value) => {
                    $("#" + key).css("width", value + "%")
                    $("#" + key).attr("aria-valuenow", value)
                    $("#" + key).html(value + "%")
                });

                $.each(result.biography, (key, value) => {
                    $("#" + key).html(value)
                });

                $.each(result.appearance, (key, value) => {
                    $("#" + key).html(value)
                });

                $.each(result.work, (key, value) => {
                    $("#" + key).html(value)
                });

                $.each(result.connections, (key, value) => {
                    $("#" + key).html(value)
                });

            }
            else if (result.response === "error") {
                alert("Cannot find the superhero!");
            }
        },
        complete: function () {
            $('#loadingModal').modal('hide');
            $('#SuperheroModal').modal('show');
        },
    });
}