# Superheros

## Getting Started
This application is developed in Ubuntu.  
  
Backend is developed using CPP with Listening Port: **8558** and front end is developed in nodejs, running port is **3000**.

### Prerequisites
Install following tools/packages:
```
sudo apt-get install build-essential

sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get install nodejs

sudo apt-get install libssl-dev
sudo apt-get install ninja-build
sudo apt-get install libcurl4-openssl-dev

version=3.16
build=1
mkdir ~/temp
cd ~/temp
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/
./bootstrap
make -j$(nproc)
sudo make install
```

### Running Server Application
Go to the directory named **server**. Then,
```
cd build/
cmake -GNinja ..
ninja

cd ..
cd out
./http_server
```
Now Server is up and running at port **8558**.

### Running Client Application
Go to the directory named **client**. Then,
```
npm install 
npm start
```

Now the superhero app is up and running in port **3000**.

On web browser, go to URL **http://localhost:3000**