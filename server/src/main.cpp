#include "mongoose.h"
#include <string>
#include <iostream>
#include <curl/curl.h>

using namespace std;

mg_mgr mgr;
mg_connection *nc;

const char *port = "8558";

const string accessToken = "626748398068089";
const string url = "https://superheroapi.com/api/";

size_t printString(void *ptr, size_t size, size_t count, void *stream)
{
    ((string *)stream)->append((char *)ptr, 0, size * count);
    return size * count;
}

string fetchData(string uri)
{
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    string response;

    string newurl = url + accessToken + uri;
    puts("Fetching from");
    puts(newurl.c_str());
    curl_easy_setopt(curl, CURLOPT_URL, newurl.c_str());
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, printString);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
    res = curl_easy_perform(curl);
    curl_easy_cleanup(curl);
    return response;
}

void evHandler(mg_connection *nc, int ev, void *ev_data)
{
    if (ev == MG_EV_HTTP_REQUEST)
    {
        struct http_message *http = (struct http_message *)ev_data;
        string uri;
        uri.assign(http->uri.p, http->uri.len);
        mg_printf(nc, "HTTP/1.1 200 OK\r\n"
                      "Content-Type: application/json\r\n"
                      "Access-Control-Allow-Origin: *\r\n"
                      "Access-Control-Allow-Methods: GET\r\n"
                      "Access-Control-Allow-Headers: Content-Type\r\n"
                      "Connection: close\r\n"
                      "\r\n"
                      "%s",
                  fetchData(uri).c_str());
        nc->flags |= MG_F_SEND_AND_CLOSE;
    }
}

int main()
{
    mg_mgr_init(&mgr, NULL);
    nc = mg_bind(&mgr, port, evHandler);
    puts("The server is listening on: https://localhost:8558");
    mg_set_protocol_http_websocket(nc);
    while (true)
        mg_mgr_poll(&mgr, 1000);
    mg_mgr_free(&mgr);
    return 0;
}